from typing import Union

from injector import Injector

from bootstrap import configure_postgres
from models.Flight import Flight
from services.postgres import PostgresProvider
import uuid

_store = Injector(configure_postgres).get(PostgresProvider)


def search(
    destination: Union[str, None] = None,
    origin: Union[str, None] = None,
    limit: int = 10,
) -> dict:
    return _store.search(destination, origin, limit)


def get_one(flight_id: str) -> dict:
    return _store.get_one(flight_id)


def post(flight: dict):
    return _store.post(
        Flight(
            id=str(uuid.uuid4()),
            origin=flight["origin"],
            destination=flight["destination"],
            airline=flight["airline"],
            departure=flight["departure"],
        )
    )
