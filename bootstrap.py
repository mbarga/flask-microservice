# https://stackoverflow.com/questions/31678827/what-is-a-pythonic-way-for-dependency-injection
import datetime
import os

from injector import Binder

from services.dictprovider import DictProvider
from services.postgres import PostgresProvider


def configure_postgres(binder: Binder) -> None:
    binder.bind(
        PostgresProvider,
        to=PostgresProvider(
            host=os.getenv("POSTGRES_HOST"),
            port=os.getenv("POSTGRES_PORT"),
            user= os.getenv("POSTGRES_USER"),
            password=os.getenv("POSTGRES_PASSWORD"),
            db=os.getenv("POSTGRES_DB")
        )
    )


def configure_dict(binder: Binder) -> None:
    binder.bind(
        DictProvider,
        to=DictProvider(
            [
                {
                    "id": "abc123",
                    "airline": "Eurowings",
                    "origin": "BERLIN",
                    "destination": "TEXAS",
                    "departure": datetime.datetime.now(),
                }
            ]
        ),
    )
