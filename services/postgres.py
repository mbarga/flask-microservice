import logging
from typing import Union

import psycopg2
from psycopg2.extras import RealDictCursor

from models import Flight

logger = logging.getLogger(__name__)

FLIGHTS_TABLE = "flights"


class PostgresProvider:
    def __init__(self, host: str, port: int, user: str, password: str, db: str) -> None:
        self._host = host
        self._port = port
        self._db = db
        self._user = user
        self._password = password
        self.instance: psycopg2.connect = None

    def create(self) -> psycopg2.connect:
        try:
            conn = psycopg2.connect(
                host=self._host,
                port=self._port,
                dbname=self._db,
                user=self._user,
                password=self._password,
            )
            return conn
        except psycopg2.OperationalError as e:
            logger.error(
                f"unable to connect to postgres {self._host}:{self._port}/{self._db}: <{e}>"
            )

    def connection(self):
        if not self.instance:
            logger.info(f"opening postgres connection..")
            self.instance = self.create()
        return self.instance

    def search(
        self,
        destination: Union[str, None] = None,
        origin: Union[str, None] = None,
        limit: int = 10,
    ) -> dict:
        if destination:
            search_query = f"SELECT * FROM {FLIGHTS_TABLE} WHERE destination = '{destination}' LIMIT {limit}"
        elif origin:
            search_query = (
                f"SELECT * FROM {FLIGHTS_TABLE} WHERE origin = '{origin}' LIMIT {limit}"
            )
        else:
            search_query = f"SELECT * FROM {FLIGHTS_TABLE} LIMIT {limit}"

        try:
            with self.connection().cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(search_query)
                results = cursor.fetchall()
                return {"flights": results}
        except AttributeError as e:
            logger.error(f"could not create a cursor: {e}")

    def get_one(self, flight_id: str) -> dict:
        raise NotImplementedError

    def post(self, flight: Flight) -> dict:
        insert_row = f"INSERT INTO flights VALUES ('{flight.id}', '{flight.airline}', '{flight.origin}', '{flight.destination}', '{flight.departure}');"
        logger.error(f"QUERY: {insert_row}")
        try:
            with self.connection().cursor() as cursor:
                cursor.execute(insert_row)
                return {"status": "success"}
        except AttributeError as e:
            logger.error(f"could not create a cursor: {e}")
