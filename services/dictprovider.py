class DictProvider:
    def __init__(self, flights: []):
        self._flights = flights

    def get(self, destination: str, limit: int = 5) -> dict:
        print(self)
        if not self._flights:
            return []

        return {
            "flights": [
                flight
                for flight in self._flights
                if flight["destination"] == destination
            ][:limit]
        }
