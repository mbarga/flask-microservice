from dataclasses import dataclass


@dataclass
class Flight:
    id: str
    origin: str
    destination: str
    airline: str
    departure: str
