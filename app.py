import os

import connexion
from connexion.resolver import RestyResolver
from dotenv import load_dotenv

load_dotenv()

if __name__ == "__main__":
    app = connexion.App(__name__, specification_dir="swagger/")
    app.add_api("swagger.yaml", resolver=RestyResolver("api"))
    app.run(port=os.getenv("APP_PORT"))
